// Add Record
function addNewCourse() {
    // get values
    var courseNum = $("#courseNum").val();
    var courseName = $("#name").val();
    var creditHours = $("#creditHours").val();
    var departmentBelonging = $("#department").val();

    // Add record
    $.post("controller/course/addNewCourse.php", {
        //POSTNAME : VAR NAME FROM ABOVE
        course_number: courseNum,
        course_name: courseName,
        credit_hours: creditHours,
        department: departmentBelonging
    }, function (data, status) {
        // close the popup
        $("#add_new_record_modal").modal("hide");

        // read records again
        readCourse();

        // clear fields from the popup
        $("#courseNum").val("");
        $("#name").val("");
        $("#creditHours").val("");
        $("#department").val("");

    });
}

// READ records
function readCourse() {
    $.get("controller/course/readCourse.php", {}, function (data, status) {
        $(".records_content").html(data);
    });
}

function loadNavbar(){
    $.get("nav_bar.html", {}, function(data,status){
        $(".nav_menu").html(data);
    });
}

$(document).ready(function () {
    // READ records on page load
    readCourse(); // calling functio
    loadNavbar();
});

function getCourseDetails(courseNum) {
    // Add User ID to the hidden field for furture usage
    $("#hidden_courseNum").val(courseNum);
    $.post("controller/course/readCourseDetails.php", {
            course_number: courseNum
        },
        function (data, status) {
            // PARSE json data
            var course = JSON.parse(data);
            // Assing existing values to the modal popup fields
            $("#update_courseNum").val(course.course_number);
            $("#update_courseName").val(course.course_name);
            $("#update_hours").val(course.credit_hours);
            $("#update_department").val(course.department);

        }
    );
    // Open modal popup
    $("#update_course_modal").modal("show");
}

function updateCourseDetails() {
    // get values
    var course_name = $("#update_courseName").val();
    var credit_hours = $("#update_hours").val();
    var department = $("#update_department").val();



    // get hidden field value
    var courseNum = $("#hidden_courseNum").val();

    // Update the details by requesting to the server using controller
    $.post("controller/course/updateCourseDetails.php", {
            course_number: courseNum,
            course_name: course_name,
            credit_hours: credit_hours,
            department: department
        },
        function (data, status) {
            // hide modal popup
            $("#update_course_modal").modal("hide");
            // reload Users by using readRecords();
            readCourse();
        }
    );
}

function deleteCourse(course_number) {
    var conf = confirm("Are you sure, do you really want to delete this course?");
    if (conf === true) {
        $.post("controller/course/deleteCourse.php", {
                course_number: course_number
            },
            function (data, status) {
                // reload Users by using readRecords();
                readCourse();
            }
        );
    }
}