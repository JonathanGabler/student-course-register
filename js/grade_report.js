// Add Record
function addNewGradeReport() {
    // get values
    var student_number = $("#grade_studentNum").val();
    var section_identifier = $("#grade_sectionID").val();
    var grade = $("#grade_grade").val();

    // Add record
    $.post("controller/grade_report/addNewGradeReport.php", {
        //POSTNAME : VAR NAME FROM ABOVE
        section_identifier: section_identifier,
        student_number: student_number,
        grade: grade,
    }, function (data, status) {
        // close the popup
        $("#add_new_record_modal").modal("hide");

        // read records again
        readGradeReport();

        // clear fields from the popup
        $("#grade_sectionID").val("");
        $("#grade_studentNum").val("");
        $("#grade_grade").val("");

    });
}

// READ records
function readGradeReport() {
    $.get("controller/grade_report/readGradeReport.php", {}, function (data, status) {
        $(".records_content").html(data);
    });
}
function loadNavbar(){
    $.get("nav_bar.html", {}, function(data,status){
        $(".nav_menu").html(data);
    });
}

$(document).ready(function () {
    // READ records on page load
    readGradeReport(); // calling function
    loadNavbar();
});

function getGradeReportDetails(student_number, section_identifier, grade) {
    // Add User ID to the hidden field for furture usage
    $("#hidden_grade_sectionID").val(section_identifier);
    $("#hidden_grade_studentNum").val(student_number);
    $("#hidden_grade_grade").val(grade);
    $.post("controller/grade_report/readGradeReportDetails.php", {
            student_number: student_number,
            section_identifier: section_identifier,
            grade: grade
        },
        function (data, status) {
            // PARSE json data
            var selection = JSON.parse(data);
            // Assing existing values to the modal popup fields
            $("#update_grade_StudentNum").val(selection.student_number);
            $("#update_grade_sectionID").val(selection.section_identifier);
            $("#update_grade_grade").val(selection.grade);
        }
    );
    // Open modal popup
    $("#update_grade_modal").modal("show");
}

function updateGrade() {
    // get values
    var student_number = $("#update_grade_StudentNum").val();
    var section_identifier = $("#update_grade_sectionID").val();
    var grade = $("#update_grade_grade").val();
    var previous_student_number = $("#hidden_grade_studentNum").val();
    var previous_section_ID = $("#hidden_grade_sectionID").val();
    var previous_grade = $("#hidden_grade_grade").val();

    // get hidden field value
    //var section_identifier = $("#hidden_section_id").val();

    // Update the details by requesting to the server using controller
    try {
        $.post("controller/grade_report/updateGradeReport.php", {
                student_number: student_number,
                section_identifier: section_identifier,
                grade: grade,
                previous_student_number: previous_student_number,
                previous_section_identifier: previous_section_ID,
                previous_grade: previous_grade
            },
            function (data, status) {
                // hide modal popup
                $("#update_grade_modal").modal("hide");
                // reload Users by using readRecords();
                readGradeReport();
            }
        );
    }
    catch (e) {
        console.log( 'Debug Objects:'+ e );
    }

}

function deleteGradeReport(student_number, section_identifier, grade) {
    var conf = confirm("Are you sure, do you really want to delete this course?");
    if (conf === true) {
        $.post("controller/grade_report/deleteGradeReport.php", {
                student_number: student_number,
                section_identifier: section_identifier,
                grade: grade
            },
            function (data, status) {
                // reload Users by using readRecords();
                readGradeReport();
            }
        );
    }
}