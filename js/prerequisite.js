// Add Record
function addPrerequisite() {
    // get values
    var course_number = $("#prerequisite_courseNum").val();
    var prerequisite_number = $("#prerequisite_prerequisiteNum").val();

    // Add record
    $.post("controller/prerequisite/addPrerequisite.php", {
        //POSTNAME : VAR NAME FROM ABOVE
        course_number: course_number,
        prerequisite_number: prerequisite_number
    }, function (data, status) {
        // close the popup
        $("#add_new_record_modal").modal("hide");

        // read records again
        readPrerequisite();

        // clear fields from the popup
        $("#prerequisite_prerequisiteNum").val("");
        $("#prerequisite_courseNum").val("");

    });
}

// READ records
function readPrerequisite() {
    $.get("controller/prerequisite/readPrerequisite.php", {}, function (data, status) {
        $(".records_content").html(data);
    });
}

function loadNavbar(){
    $.get("nav_bar.html", {}, function(data,status){
        $(".nav_menu").html(data);
    });
}

$(document).ready(function () {
    // READ records on page load
    readPrerequisite(); // calling function
    loadNavbar();
});

function getPrerequisiteDetails(course_number, prerequisite_number) {
    // Add User ID to the hidden field for furture usage
    $("#hidden_prerequisite_courseNum").val(course_number);
    $("#hidden_prerequisite_prerequisiteNum").val(prerequisite_number);
    $.post("controller/prerequisite/readPrerequisiteDetails.php", {
            course_number: course_number,
            prerequisite_number: prerequisite_number,
        },
        function (data, status) {
            // PARSE json data
            var selection = JSON.parse(data);
            // Assing existing values to the modal popup fields
            $("#update_prerequisite_courseNum").val(selection.course_number);
            $("#update_prerequisite_prerequisiteNum").val(selection.prerequisite_number);
        }
    );
    // Open modal popup
    $("#update_prerequisite_modal").modal("show");
}

function updatePrerequisite() {
    // get values
    var course_number = $("#update_prerequisite_courseNum").val();
    var prerequisite_number = $("#update_prerequisite_prerequisiteNum").val();
    var previous_course_number = $("#hidden_prerequisite_courseNum").val();
    var previous_prerequisite_number = $("#hidden_prerequisite_prerequisiteNum").val();

    // Update the details by requesting to the server using controller
    try {
        $.post("controller/prerequisite/updatePrerequisite.php", {
                course_number: course_number,
                prerequisite_number: prerequisite_number,
                previous_course_number: previous_course_number,
                previous_prerequisite_number: previous_prerequisite_number,
            },
            function (data, status) {
                // hide modal popup
                $("#update_prerequisite_modal").modal("hide");
                // reload Users by using readRecords();
                readPrerequisite();
            }
        );
    }
    catch (e) {
        console.log( 'Debug Objects:'+ e );
    }

}

function deletePrerequisite(course_number, prerequisite_number) {
    var conf = confirm("Are you sure, do you really want to delete this course?");
    if (conf === true) {
        $.post("controller/prerequisite/deletePrerequisite.php", {
                course_number: course_number,
                prerequisite_number: prerequisite_number,
            },
            function (data, status) {
                // reload Users by using readRecords();
                readPrerequisite();
            }
        );
    }
}