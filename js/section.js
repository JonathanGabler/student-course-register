// Add Record
function addNewSection() {
    // get values
    var section_identifier = $("#section_identifier").val();
    var course_number = $("#course_number").val();
    var semester = $("#semester").val();
    var year = $("#year").val();
    var instructor = $("#instructor").val();

    // Add record
    $.post("controller/section/addNewSection.php", {
        //POSTNAME : VAR NAME FROM ABOVE
        section_identifier: section_identifier,
        course_number: course_number,
        semester: semester,
        year: year,
        instructor: instructor
    }, function (data, status) {
        // close the popup
        $("#add_new_record_modal").modal("hide");

        // read records again
        readSections();

        // clear fields from the popup
        $("#section_identifier").val("");
        $("#course_number").val("");
        $("#semester").val("");
        $("#year").val("");
        $("#instructor").val("");


    });
}

// READ records
function readSections() {
    $.get("controller/section/readSections.php", {}, function (data, status) {
        $(".records_content").html(data);
    });
}
function loadNavbar(){
    $.get("nav_bar.html", {}, function(data,status){
        $(".nav_menu").html(data);
    });
}

$(document).ready(function () {
    // READ records on page load
    readSections(); // calling function
    loadNavbar();
});

function getSectionDetails(section_identifier) {
    // Add User ID to the hidden field for furture usage
    $("#hidden_section_id").val(section_identifier);
    $.post("controller/section/readSectionDetails.php", {
            section_identifier: section_identifier
        },
        function (data, status) {
            // PARSE json data
            var section = JSON.parse(data);
            // Assing existing values to the modal popup fields
            $("#update_section_identifier").val(section.section_identifier);
            $("#update_course_number").val(section.course_number);
            $("#update_semester").val(section.semester);
            $("#update_year").val(section.year);
            $("#update_instructor").val(section.instructor);

        }
    );
    // Open modal popup
    $("#update_user_modal").modal("show");
}

function updateSection() {
    // get values
    var course_number = $("#update_course_number").val();
    var semester = $("#update_semester").val();
    var year = $("#update_year").val();
    var instructor = $("#update_instructor").val();



    // get hidden field value
    var section_identifier = $("#hidden_section_id").val();

    // Update the details by requesting to the server using controller
    $.post("controller/section/updateSection.php", {
            section_identifier: section_identifier,
            course_number: course_number,
            semester: semester,
            year: year,
            instructor: instructor
        },
        function (data, status) {
            // hide modal popup
            $("#update_user_modal").modal("hide");
            // reload Users by using readRecords();
            readSections();
        }
    );
}

function deleteSection(section_ID) {
    var conf = confirm("Are you sure, do you really want to delete this course?");
    if (conf === true) {
        $.post("controller/section/deleteSection.php", {
                section_identifier: section_ID
            },
            function (data, status) {
                // reload Users by using readRecords();
                readSections();
            }
        );
    }
}