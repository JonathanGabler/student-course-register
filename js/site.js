// Add Record
function addNewStudent() {
    // get values
    var student_number = $("#student_number").val();
    var name = $("#name").val();
    var classes = $("#class").val();
    var major = $("#major").val();

    // Add record
    $.post("controller/student/addNewStudent.php", {
        student_number: student_number,
        name: name,
        class: classes,
        major: major
    }, function (data, status) {
        // close the popup
        $("#add_new_record_modal").modal("hide");

        // read records again
        readStudents();

        // clear fields from the popup
        $("#student_number").val("");
        $("#name").val("");
        $("#class").val("");
        $("#major").val("");

    });
}

// READ records
function readStudents() {
    $.get("controller/student/readStudents.php", {}, function (data, status) {
        $(".records_content").html(data);
    });
}

function loadNavbar(){
    $.get("nav_bar.html", {}, function(data,status){
        $(".nav_menu").html(data);
    });
}

$(document).ready(function () {
    // READ records on page load
    readStudents(); // calling function
    loadNavbar();
});

function getStudentDetails(student_number) {
    // Add User ID to the hidden field for furture usage
    $("#hidden_user_id").val(student_number);
    $.post("controller/student/readStudentDetails.php", {
            student_number: student_number
        },
        function (data, status) {
            // PARSE json data
            var user = JSON.parse(data);
            // Assing existing values to the modal popup fields
            $("#update_student_number").val(user.student_number);
            $("#update_name").val(user.name);
            $("#update_class").val(user.class);
            $("#update_major").val(user.major);

        }
    );
    // Open modal popup
    $("#update_user_modal").modal("show");
}

function UpdateUserDetails() {
    // get values
    var name = $("#update_name").val();
    var classes = $("#update_class").val();
    var major = $("#update_major").val();


    // get hidden field value
    var student_number = $("#hidden_user_id").val();

    // Update the details by requesting to the server using controller
    $.post("controller/student/updateStudentDetails.php", {
            student_number: student_number,
            name: name,
            class: classes,
            major: major
        },
        function (data, status) {
            // hide modal popup
            $("#update_user_modal").modal("hide");
            // reload Users by using readRecords();
            readStudents();
        }
    );
}

function DeleteStudent(student_number) {
    var conf = confirm("Are you sure, do you really want to delete User?");
    if (conf === true) {
        $.post("controller/student/deleteStudent.php", {
                student_number: student_number
            },
            function (data, status) {
                // reload Users by using readRecords();
                readStudents();
            }
        );
    }
}