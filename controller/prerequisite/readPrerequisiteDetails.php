<?php
/**
 * Created by PhpStorm.
 * User: JonGa
 * Date: 3/31/2019
 * Time: 12:32 AM
 */
// include Database connection file
include("../db_connection.php");

// check request
if(isset($_POST['course_number']) && isset($_POST['course_number']) != ""
    && isset($_POST['prerequisite_number']) && isset($_POST['prerequisite_number']) != ""
)
{
    // get User ID
    $course_number = $_POST['course_number'];
    $prerequisite_number = $_POST['prerequisite_number'];

    // Get User Details
    $query = "SELECT * FROM prerequisite WHERE course_number = '$course_number' 
      AND prerequisite_number = '$prerequisite_number'";
    if (!$result = mysqli_query($con, $query)) {
        exit(mysqli_error($con));
    }
    $response = array();
    if(mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $response = $row;
        }
    }
    else
    {
        $response['status'] = 200;
        $response['message'] = "Data not found!";
    }
    // display JSON data
    echo json_encode($response);
}
else
{
    $response['status'] = 200;
    $response['message'] = "Invalid Request!";
}