<?php
/**
 * Created by PhpStorm.
 * User: JonGa
 * Date: 3/31/2019
 * Time: 1:24 AM
 */

// include Database connection file
include("../db_connection.php");

// check request
if(isset($_POST))
{

    // get values
    $student_number = $_POST['student_number'];
    $section_identifier = $_POST['section_identifier'];
    $grade = $_POST['grade'];
    $previous_grade = $_POST['previous_grade'];
    $previous_student_number = $_POST['previous_student_number'];
    $previous_section_identifier = $_POST['previous_section_identifier'];

    // Update User details
    try {
        $query = "UPDATE grade_report SET student_number = '$student_number', section_identifier = '$section_identifier', grade = '$grade' 
          WHERE student_number = '$previous_student_number'
          AND section_identifier = '$previous_section_identifier'
          AND grade = '$previous_grade'";
        echo $query;
        if (!$result = mysqli_query($con, $query)) {
            exit(mysqli_error($con));
        }
        else {
            echo "Number of rows affected: " . mysql_affected_rows() . "<br>";
        }
    }
    catch (exception $e) {
        echo $e->getMessage();
        echo "<script>console.log( 'Debug Objects: " . $e . "' );</script>";
    }

}