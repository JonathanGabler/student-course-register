<?php
/**
 * Created by PhpStorm.
 * User: JonGa
 * Date: 3/30/2019
 * Time: 10:48 PM
 */

// include Database connection file
include("../db_connection.php");

// Design initial table header
$data = '<table class="table table-bordered table-striped">
						<tr>
							<th>Student No.</th>
							<th>Section ID</th>
							<th>Grade</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>';

$query = "SELECT * FROM grade_report";

if (!$result = mysqli_query($con, $query)) {
    exit(mysqli_error($con));
}

// if query results contains rows then fetch those rows
if(mysqli_num_rows($result) > 0)
{
    $number = 1;
    while($row = mysqli_fetch_assoc($result))
    {
        $data .= '<tr>
				<td>'.$row['student_number'].'</td>
				<td>'.$row['section_identifier'].'</td>
				<td>'.$row['grade'].'</td>
				<td>
					<button onclick="getGradeReportDetails('.$row['student_number'].', '.$row['section_identifier'].',\''.$row['grade'].'\')" class="btn btn-sm btn-outline-primary"><i class="fas fa-user-edit"></i></button>
				</td>
				<td>
					<button onclick="deleteGradeReport('.$row['student_number'].', '.$row['section_identifier'].',\''.$row['grade'].'\')" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
				</td>
    		</tr>';
        $number++;
    }
}
else
{
    // records now found
    $data .= '<tr><td colspan="6">Records not found!</td></tr>';
}

$data .= '</table>';

echo $data;
?>