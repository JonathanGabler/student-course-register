<?php
/**
 * Created by PhpStorm.
 * User: JonGa
 * Date: 3/30/2019
 * Time: 10:48 PM
 */

// include Database connection file
include("../db_connection.php");

// Design initial table header
$data = '<table class="table table-bordered table-striped">
						<tr>
							<th>Course No.</th>
							<th>Name</th>
							<th>Credit Hours</th>
							<th>Department</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>';

$query = "SELECT * FROM course";

if (!$result = mysqli_query($con, $query)) {
    exit(mysqli_error($con));
}

// if query results contains rows then fetch those rows
if(mysqli_num_rows($result) > 0)
{
    $number = 1;
    while($row = mysqli_fetch_assoc($result))
    {
        $data .= '<tr>
				<td>'.$row['course_number'].'</td>
				<td>'.$row['course_name'].'</td>
				<td>'.$row['credit_hours'].'</td>
				<td>'.$row['department'].'</td>
				<td>
					<button onclick="getCourseDetails(\''.$row['course_number'].'\')" class="btn btn-sm btn-outline-primary"><i class="fas fa-user-edit"></i></button>
				</td>
				<td>
					<button onclick="deleteCourse(\''.$row['course_number'].'\')" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
				</td>
    		</tr>';
        $number++;
    }
}
else
{
    // records now found
    $data .= '<tr><td colspan="6">Records not found!</td></tr>';
}

$data .= '</table>';

echo $data;
?>