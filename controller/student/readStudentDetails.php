<?php
/**
 * Created by PhpStorm.
 * User: JonGa
 * Date: 3/31/2019
 * Time: 12:32 AM
 */
// include Database connection file
include("../db_connection.php");

// check request
if(isset($_POST['student_number']) && isset($_POST['student_number']) != "")
{
    // get User ID
    $student_number = $_POST['student_number'];

    // Get User Details
    $query = "SELECT * FROM student WHERE student_number = '$student_number'";
    if (!$result = mysqli_query($con, $query)) {
        exit(mysqli_error($con));
    }
    $response = array();
    if(mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $response = $row;
        }
    }
    else
    {
        $response['status'] = 200;
        $response['message'] = "Data not found!";
    }
    // display JSON data
    echo json_encode($response);
}
else
{
    $response['status'] = 200;
    $response['message'] = "Invalid Request!";
}