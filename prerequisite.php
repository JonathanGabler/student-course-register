
<!--
 * Created by PhpStorm.
 * User: JonGa
 * Date: 3/30/2019
 * Time: 8:57 PM
 -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Grade Report</title>

    <!-- Bootstrap CSS File  -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>
<body>

<!-- Content Section -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>University DB </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h5>Records: Prerequisite Table</h5>
            <div class="nav_menu"></div>
            <br>
            <div class="records_content"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#add_new_record_modal">Add New Record</button>
            </div>
        </div>
    </div>
</div>
<!-- /Content Section -->


<!-- Bootstrap Modals -->
<!-- Modal - Add New Record/User -->
<div class="modal fade" id="add_new_record_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Record</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label for="first_name">Course Number</label>
                    <input type="text" id="prerequisite_courseNum" placeholder="Course #" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="first_name">Prerequisite Number</label>
                    <input type="text" id="prerequisite_prerequisiteNum" placeholder="Prerequisite #" class="form-control"/>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-sm btn-success" onclick="addPrerequisite()">Add Record</button>
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->

<!-- Modal - Update User details -->
<div class="modal fade" id="update_prerequisite_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Update</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label for="first_name">Course Number</label>
                    <input type="text" id="update_prerequisite_courseNum" placeholder="Course #" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="first_name">Prerequisite Number</label>
                    <input type="text" id="update_prerequisite_prerequisiteNum" placeholder="Prerequisite #" class="form-control"/>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-sm btn-success" onclick="updatePrerequisite()" >Save Changes</button>
                <input type="hidden" id="hidden_prerequisite_courseNum">
                <input type="hidden" id="hidden_prerequisite_prerequisiteNum">
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->

<!-- Jquery JS file -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Bootstrap JS file -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<!-- Custom JS file -->
<script type="text/javascript" src="js/prerequisite.js"></script>
</body>
</html>